﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WebAPIServiceLayer.Repository;
using WebAPIServiceLayer.Models;
using System.Collections.Generic;

namespace CredoServiceLayer.Tests.Repository
{
    [TestClass]
    public class UnitTestTasksController
    {        
        [TestMethod]
        public void GetTaskByIdFromRepository()
        {
            //Arrange
            //Create Fake Object
            var fakeObject = new Mock<ITaskRepository>();

            //Set the Mock Configuration
            var task = new Task { Id = 1, Name = "Check phone type", Description = "What is the phone type", IsComplete = true };
            fakeObject.Setup(x => x.Get(1)).Returns(task);

            //Act
            var result = fakeObject.Object.Get(1);

            //Assert
            Assert.AreEqual(result, task);
        }

        [TestMethod]
        public void GetAllTasksFromRepository()
        {
            //Arrange
            //Create mock tasks            
            var repositoryMock = new Mock<ITaskRepository>();

            var tasks = new List<Task>();

            tasks.Add(new Task { Id = 1, Name = "Check phone type", Description = "What is the phone type", IsComplete = true });
            tasks.Add(new Task { Id = 2, Name = "Subscriber Type", Description = "What is the subscriber is the customer?", IsComplete = false });
            tasks.Add(new Task { Id = 3, Name = "Detailed description of the proble", Description = "Detailed description of the problem/issue", IsComplete = true });
            tasks.Add(new Task { Id = 4, Name = "Problem statement", Description = "How long did the customer have this problem?", IsComplete = true });

            repositoryMock.Setup(x => x.GetAll()).Returns(tasks);            

            //Act
            //Use the mock repository to get all tasks
            var result = repositoryMock.Object.GetAll();

            //Assert            
            Assert.AreEqual(4, result.Count);            
        }
    }
}
