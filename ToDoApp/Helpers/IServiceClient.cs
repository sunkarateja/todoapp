﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToDoApp.Models;

namespace ToDoApp.Helpers
{
    public interface IServiceClient
    {
        IEnumerable<ToDoDataModel> GetAll();
        ToDoDataModel Get(int id);

        void AddTask(ToDoDataModel task);
        void UpdateTask(ToDoDataModel task);

        void RemoveTask(int id);
    }
}
