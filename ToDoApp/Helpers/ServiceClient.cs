﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoApp.Models;
using RestSharp;
using System.Net;

namespace ToDoApp.Helpers
{
    public class ServiceClient : IServiceClient
    {
        private readonly RestClient _client;
        private readonly string _url = "http://localhost:52193/";

        public ServiceClient()
        {
            _client = new RestClient(_url);
        }

        public IEnumerable<ToDoDataModel> GetAll()
        {
            var request = new RestRequest("api/tasks", Method.GET) {RequestFormat = DataFormat.Json};

            var response = _client.Execute<List<ToDoDataModel>>(request);

            if (response.Data == null)
                throw new Exception(response.ErrorMessage);

            return response.Data;
        }

        public ToDoDataModel Get(int id)
        {
            var request = new RestRequest("api/tasks/{id}", Method.GET) {RequestFormat = DataFormat.Json};

            request.AddParameter("id", id, ParameterType.UrlSegment);
            
            var response = _client.Execute<ToDoDataModel>(request);

            if (response.Data == null)
                throw new Exception(response.ErrorMessage);

            return response.Data;
        }

        public void AddTask(ToDoDataModel serverData)
        {
            var request = new RestRequest("api/tasks", Method.POST) {RequestFormat = DataFormat.Json};
            request.AddBody(serverData);

            var response = _client.Execute<ToDoDataModel>(request);

            if (response.StatusCode != HttpStatusCode.Created)
                throw new Exception(response.ErrorMessage);

        }

        public void UpdateTask(ToDoDataModel serverData)
        {
            var request = new RestRequest("api/tasks/{id}", Method.PUT) {RequestFormat = DataFormat.Json};
            request.AddParameter("id", serverData.Id, ParameterType.UrlSegment);
            request.AddBody(serverData);

            var response = _client.Execute<ToDoDataModel>(request);

            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new Exception(response.ErrorMessage);
        }

        public void RemoveTask(int id)
        {
            var request = new RestRequest("api/tasks/{id}", Method.DELETE);
            request.AddParameter("id", id, ParameterType.UrlSegment);
           
            var response = _client.Execute<ToDoDataModel>(request);

            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new Exception(response.ErrorMessage);
        }
    }    
}