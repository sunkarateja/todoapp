﻿using ToDoApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoApp.Helpers;

namespace CredoTasksApp.Controllers
{    
    public class TasksController : Controller
    {
        static readonly IServiceClient RestClient = new ServiceClient();
        //
        // GET: /tasks/

        public ActionResult GetAllTasks()
        {
            return View(RestClient.GetAll());
        }

        //
        // GET: /tasks/Details/5

        public ActionResult Details(int id)
        {
            return View(RestClient.Get(id));
        }

        //
        // GET: /tasks/AddTask

        public ActionResult AddTask()
        {
            return View();
        }

        //
        // POST: /tasks/AddTask

        [HttpPost]
        public ActionResult AddTask(ToDoDataModel serverData)
        {
            try
            {
                RestClient.AddTask(serverData);
                return RedirectToAction("GetAllTasks");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /tasks/Edit/5

        public ActionResult EditTask(int id)
        {
            return View(RestClient.Get(id));
        }

        //
        // POST: /tasks/Edit/5

        [HttpPost]
        public ActionResult EditTask(ToDoDataModel serverData)
        {
            try
            {
                RestClient.UpdateTask(serverData);

                return RedirectToAction("GetAllTasks");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /tasks/Delete/5

        public ActionResult Delete(int id)
        {
            return View(RestClient.Get(id));
        }

        //
        // POST: /tasks/Delete/5

        [HttpPost]
        public ActionResult Delete(ToDoDataModel serverData)
        {
            try
            {
                RestClient.RemoveTask(serverData.Id);

                return RedirectToAction("GetAllTasks");
            }
            catch
            {
                return View();
            }
        }
    }
}
