﻿using WebAPIServiceLayer.Models;
using WebAPIServiceLayer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPIServiceLayer.Controllers
{
    public class TasksController : ApiController
    {
        //private ITaskRepository _repository;

        //public TasksController(ITaskRepository repository)
        //{
        //    if (repository == null)
        //        throw new ArgumentNullException("repository");

        //    _repository = repository;
        //}

        static TaskRepository _repository;

        private TasksController()
        {
            _repository = new TaskRepository();
        }     

        //Relative URI= /api/Tasks
        public IEnumerable<Task> GetAllTasks()
        {
            return _repository.GetAll();
        }
        //Relative URI /api/Tasks/id
        public Task GetTask(int id)
        {
            Task Task = _repository.Get(id);
            if (Task == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return Task;
        }

        public HttpResponseMessage PostTask(Task Task)
        {
            Task = _repository.AddTask(Task);
            var response = Request.CreateResponse<Task>(HttpStatusCode.Created, Task);
            string uri = Url.Route(null, new { id = Task.Id });
            response.Headers.Location = new Uri(Request.RequestUri, uri);
            return response;
        }

        public void PutTask(int id, Task Task)
        {
            Task.Id = id;
            if (!_repository.UpdateTask(Task))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
        }

        public HttpResponseMessage DeleteTask(int id)
        {
            _repository.RemoveTask(id);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}
