﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPIServiceLayer.Models;
using System.Data;

namespace WebAPIServiceLayer.Repository
{
    public class TaskRepository : ITaskRepository, IDisposable
    {
        private CredoDataEntities context;

        public TaskRepository(CredoDataEntities context)
        {
            this.context = context;
        }
        public TaskRepository()
        { }

        public List<Task> GetAll()
        {
          using (CredoDataEntities db = new CredoDataEntities())
            { 
             db.Database.Connection.Open();
             var tasks = db.Tasks;
             var result = (from o in db.Tasks
                          select o).ToList();            
             return result;
            }           
        }

        public Task Get(int id)
        {
            return context.Tasks.Find(id);
        }

        public Task AddTask(Task task)
        {            
           using (CredoDataEntities db = new CredoDataEntities())
             {
                db.Database.Connection.Open();
                db.Tasks.Add(task);
                db.SaveChanges();
                return task;
             }                     
        }

        public void RemoveTask(int Id)
        {
            using (CredoDataEntities db = new CredoDataEntities())
            {
                Task task = context.Tasks.Find(Id);
                db.Tasks.Remove(task);
                db.SaveChanges();
            }
        }

        public bool UpdateTask(Task task)
        {
            using (CredoDataEntities db = new CredoDataEntities())
            { 
                   db.Entry(task).State = EntityState.Modified;
                   db.SaveChanges();
                   return true;
            }            
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }     
    }
}