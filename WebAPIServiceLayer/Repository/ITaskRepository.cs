﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebAPIServiceLayer.Models;

namespace WebAPIServiceLayer.Repository
{
    public interface ITaskRepository : IDisposable
    {              
        List<Task> GetAll();
        Task Get(int id);

        Task AddTask(Task task); 
        bool UpdateTask(Task task);        

        void RemoveTask(int id);
    }
}
